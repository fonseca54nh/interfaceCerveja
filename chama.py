from time import sleep           # Allows us to call the sleep function to slow down our loop
import RPi.GPIO as GPIO           # Allows us to call our GPIO pins and names it just GPIO
import os

def setup():
     # porta 22 panela 2
    GPIO.setmode(GPIO.BCM)                   # Set's GPIO pins to BCM GPIO numbering
    INPUT_PIN = 23                           # Sets our input pin, in this example I'm connecting our button to pin 4. Pin 0 is the SDA pin so I avoid using it for sensors/buttons
    INPUT_PIN2 = 24                          # conferir se pode ser esta porta equivalente a GPIO22     
    INPUT_PIN3 = 25                          # conferir se pode ser esta porta         
    GPIO.setup(INPUT_PIN, GPIO.IN)           # Set our input pin to be an input
    GPIO.setup(INPUT_PIN2, GPIO.IN)
    GPIO.setup(INPUT_PIN3, GPIO.IN)
# Start a loop that never ends
def chamaLigada():
    INPUT_PIN = 23 
    if(GPIO.input(INPUT_PIN) == GPIO.HIGH): # Physically read the pin now
        return 0
    if (GPIO.input(INPUT_PIN) == GPIO.LOW):
        return 1
    sleep(1);           # Sleep for a full second before restarting our loop

def chamaLigada2(): #Chama da panela2 (fervura)
    INPUT_PIN2 = 24
    if(GPIO.input(INPUT_PIN2) == GPIO.HIGH): # Physically read the pin now
        return 0
    if (GPIO.input(INPUT_PIN2) == GPIO.LOW):
        return 1
    sleep(1);           # Sleep for a full second before restarting our loop

def chamaLigada3(): #Chama da panela3 (somente agua)
    INPUT_PIN3 = 25 
    if(GPIO.input(INPUT_PIN3) == GPIO.HIGH): # Physically read the pin now
        return 0
    if (GPIO.input(INPUT_PIN3) == GPIO.LOW):
        return 1
    sleep(1);           # Sleep for a full second before restarting our loop

