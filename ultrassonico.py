#!/usr/bin/python

# Import required Python libraries
import os
import time               # library for time reading time
import RPi.GPIO as GPIO   # library to control Rpi GPIOs

GPIO.setwarnings(False)
# Constantes
pi = 3.14159265358979323846
Raio = 25.0
Altura = 77.1
VolumeBalde = pi*(Raio**2)*Altura
Volume2 = VolumeBalde/2

def read(num):
    
   
    #Setups:
    # We will be using the BCM GPIO numbering
    GPIO.setmode(GPIO.BCM)

    # Select which GPIOs you will use
    if num == 1: #Sensor ultra panela 1 mosturacao
        GPIO_TRIGGER = 6
        GPIO_ECHO    = 5
    elif num == 2: #Sensor ultra panela 2 fervura
        GPIO_TRIGGER = 3
        GPIO_ECHO    = 4
        #Ajustar para o  segundo sensor ultrassonico 
    elif num == 3: #Sensor ultra panela 3 somente agua
        GPIO_TRIGGER = 8
        GPIO_ECHO    = 7
    

    # Set TRIGGER to OUTPUT mode
    GPIO.setup(GPIO_TRIGGER,GPIO.OUT)
    # Set ECHO to INPUT mode
    GPIO.setup(GPIO_ECHO,GPIO.IN)
    
    # Set TRIGGER to LOW
    GPIO.output(GPIO_TRIGGER, False)
        
    # Let the sensor settle for a while
    time.sleep(0.5)
    
    #Ajustar para o  segundo sensor ultrassonico 

    # Send 10 microsecond pulse to TRIGGER
    GPIO.output(GPIO_TRIGGER, True) # set TRIGGER to HIGH
    time.sleep(0.00002) # wait 20 microseconds
    GPIO.output(GPIO_TRIGGER, False) # set TRIGGER back to LOW

    # Create variable start and give it current time
    start = time.time()
    # Refresh start value until the ECHO goes HIGH = until the wave is send
    while GPIO.input(GPIO_ECHO)==0:
        start = time.time()
    # Assign the actual time to stop variable until the ECHO goes back from HIGH to LOW
    while GPIO.input(GPIO_ECHO)==1:
        stop = time.time()

    # Calculate the time it took the wave to travel there and back
    measuredTime = stop - start
    # Calculate the travel distance by multiplying the measured time by speed of sound
    distanceBothWays = measuredTime * 33150 # cm/s in 20 degrees Celsius
    # Divide the distance by 2 to get the actual distance from sensor to obstacle
    distance = distanceBothWays / 2
    rounddistance = round(distance, 2)
    rounddistance = rounddistance + 4      

    # Print the distance
    #print("Distance : {0:5.1f}cm\r".format(distance))
    #Distance 20 - 600
    
    time.sleep(0.000002) # wait 10 microseconds
    liquido = Altura-rounddistance
    if liquido < 0.0:
        liquido = 0.0
    volume = pi*(Raio*Raio)*(liquido)*0.001
    #print("Distance: ",round(rounddistance, 2),"cm","  Volume: ",round(volume, 2),"L","  Liquido: ", round(liquido,2),"cm", end='\r')
    #print(str(round(rounddistance, 2))   +  "\n" + str(round(volume, 2)) + "\n" + str(round(liquido,2)))
    time.sleep(0.3)
    # Reset GPIO settings
    GPIO.cleanup()
    return volume

def MediaVolume(num):
    vol = 0
    media = 0
    for x in range(5):
        #Pega cinco vezes o volume para fazer uma media removendo valores anomalos
        vol = read(num) + vol
    media = vol/5
    return media
