from multiprocessing.pool import IMapIterator
import rele
import chama
import temperatura
import ultrassonico
import time
from datetime import datetime
from re import S
import time
from time import sleep

rele.setup()
temperatura.setup()
chama.setup()

#print('Portas: 17 Centelhador - 13 - 19 gas2 - 26 - 12 gas3 - 16 Bomba- 20 gas1 - 21 Agua')
Centelhador1 = 17
Centelhador2 = 17
Centelhador3 = 17
Gas2 = 19
Gas3 = 12
Gas1 = 20
BombaRT= 16 #Bomba que serve para trasfega e reciculacao
BombaFlysparge = 13 #criei nova 
Agua = 21
start = 0


MostTemp  = 0 #60 #Recebe da receita em min
recircula = 0 #15 #Recebe da receita em min
Pan3Time  = 0 #15 #Recebe da receita em min
Mashout   = 0 #15 #Recebe da receita em min
Fervura   = 0 #60 #Recebe da receita em min
horribleHack  = 0
horribleHack1 = 0

def checkTemp( a ):
    minPast = 0
    minSave = 0
    dt_obj = datetime.now()
    minInit = dt_obj.strftime("%M")

    while minPast < a:
        dt_obj = datetime.now()
        minNow = dt_obj.strftime("%M")
        if int(minInit) <= int(minNow):    
            minPast =  int(minNow) - int(minInit)
            minSave = int(minNow)
        elif minSave > int(minNow):
            a = a - minPast
            a = a - (60 - minSave) 
            minInit = 0
            minPast =  int(minNow) - int(minInit)
        if (a - minPast) <= recircula:
            rele.liga(BombaRT) #liga a recirculacao apos 45 min
        tempi = temperatura.temp(1)
        
        if tempi >= 68: #Recebe da receita em graus
            rele.desliga(Gas1)
        if tempi <= 66 and chama.chamaLigada() == 1: #Recebe da receita em graus
            rele.liga(Gas1)
            time.sleep(2)
            rele.liga(Centelhador1)
            time.sleep(3)
            rele.desliga(Centelhador1)
            while chama.chamaLigada() == 0:
                rele.liga(Centelhador1)
                time.sleep(3)
                rele.desliga(Centelhador1)

def checkTemp2( a ): # panela 3 (somente agua)
    minPast = 0
    minSave = 0
    dt_obj = datetime.now()
    minInit = dt_obj.strftime("%M")

    while minPast < a:
        dt_obj = datetime.now()
        minNow = dt_obj.strftime("%M")
        if int(minInit) <= int(minNow):    
            minPast =  int(minNow) - int(minInit)
            minSave = int(minNow)
        elif minSave > int(minNow):
            a = a - minPast
            a = a - (60 - minSave) 
            minInit = 0
            minPast =  int(minNow) - int(minInit)
    
        tempi = temperatura.temp(3)
        if tempi >= 68: #Recebe da receita em graus
            rele.desliga(Gas3)
        if tempi <= 66 and chama.chamaLigada3() == 0: #Recebe da receita em graus
            rele.liga(Gas3)
            time.sleep(2)
            rele.liga(Centelhador3)
            time.sleep(3)
            rele.desliga(Centelhador3)
            while chama.chamaLigada() == 0:
                rele.liga(Centelhador3)
                time.sleep(3)
                rele.desliga(Centelhador3)

def checkTemp3( chinook1, chinook2, a ): #Alterar variaveis para panela 2 (a a 100°)
    #chinook2 = 15 #recebe da receita em min (minutos que faltem)
    minPast = 0
    minSave = 0
    dt_obj = datetime.now()
    minInit = dt_obj.strftime("%M")
    while ( minPast < a ):
        dt_obj = datetime.now()
        minNow = dt_obj.strftime("%M")
        if int(minInit) <= int(minNow):    
            minPast =  int(minNow) - int(minInit)
            minSave = int(minNow)
        elif minSave > int(minNow):
            a = a - minPast
            a = a - (60 - minSave) 
            minInit = 0
            minPast =  int(minNow) - int(minInit)
        if (a - minPast) <= chinook1:
            #notifica para adicionar o chinook2
            horribleHack = 1
        if (a - minPast) <= chinook2:
            #notifica para adicionar o chinook2
            horribleHack1 = 1

    tempi = temperatura.temp(2)
    if tempi >= 98: #Recebe da receita em gruas
        rele.desliga(Gas2)
    elif tempi <= 95 and chama.chamaLigada2() == 0: #Recebe da receita em graus
        rele.liga(Gas2)
        time.sleep(2)
        rele.liga(Centelhador2)
        time.sleep(3)
        rele.desliga(Centelhador2)
        while chama.chamaLigada2 == 0:
            rele.liga(Centelhador2)
            time.sleep(3)
            rele.desliga(Centelhador2)

def mashOut( Mashout ):
    minPast = 0
    minSave = 0
    dt_obj = datetime.now()
    minInit = dt_obj.strftime("%M")
    while minPast < Mashout:
    
        dt_obj = datetime.now()
        minNow = dt_obj.strftime("%M")
        if int(minInit) <= int(minNow):    
            minPast =  int(minNow) - int(minInit)
            minSave = int(minNow)
        elif minSave > int(minNow):
            Mashout = Mashout - minPast
            Mashout = Mashout - (60 - minSave) 
            minInit = 0
            minPast =  int(minNow) - int(minInit)

    tempi = temperatura.temp(1)
    if tempi >= 73: #Recebe da receita em graus
        rele.desliga(Gas1)
    if tempi < 73 and chama.chamaLigada() == 0: #Recebe da receita em graus
        rele.liga(Gas1)
        time.sleep(2)
        rele.liga(Centelhador1)
        time.sleep(3)
        rele.desliga(Centelhador1)
        while chama.chamaLigada() == 0:
            rele.liga(Centelhador1)
            time.sleep(3)
            rele.desliga(Centelhador1)

def checkTrasfega():
    minPast = 0
    minSave = 0
    dt_obj = datetime.now()
    minInit = dt_obj.strftime("%M")
    Trasfega = 5                                            #Tempo em min para esperar para verificacao
    check1 = ultrassonico.MediaVolume(1)
    end = 0
    while end == 0:
        check1 = ultrassonico.MediaVolume(1)
        while minPast < Trasfega:
            
            dt_obj = datetime.now()
            minNow = dt_obj.strftime("%M")
            if int(minInit) <= int(minNow):    
                minPast =  int(minNow) - int(minInit)
                minSave = int(minNow)
            elif minSave > int(minNow):
                Trasfega = Trasfega - minPast
                Trasfega = Trasfega - (60 - minSave) 
                minInit = 0
                minPast =  int(minNow) - int(minInit)
        check2 = ultrassonico.MediaVolume(1)
        if check1 <= (check2 + 2) and check1 >= (check2 - 2):
            end = 1

def abastecimentoPanela1():
    volume = ultrassonico.MediaVolume(1)             #Pega uma media do volume atual na panela 1
    rele.setup()
    rele.liga(Agua)                                 #Liga a solenoide para encher a panela 1         
    while volume <= ultrassonico.VolumeBalde:
        volume = ultrassonico.MediaVolume(1)
    rele.desliga(Agua)                              #Desliga a solenoide
#Fim do abastecimento da panela 1

#Notificar interface para mover a magueira para panela 3

def mosturacaoInit():
    #Inicio da mosturacao
    temperatura.setup()
    chama.setup()
    checkTemp( MostTemp )                           #Realiza a mosturação 
    rele.desliga(Gas1)                              #Ao finalizar a mosturação desliga o gas1
    rele.desliga(BombaRT)                           #Para a recirculacao
    #Fim da mosturacao

#Espera 10 min pro cara mover a magueira

#Inicio do abastecimento da panela 3:
def abastecimentoPanela3():
    volume = ultrassonico.MediaVolume(3)             #Pega uma media do volume atual na panela 1
    rele.liga(Agua)
    while volume <= ultrassonico.Volume2:           #Pega metade do volume que foi utilizado na panela 1
        volume = ultrassonico.MediaVolume(3)
    rele.desliga(Agua)
#Fim do abastecimento da panela 3

#Inicio do aquecimento de somente agua da panela 3
def aquecimentoPanela3():
    checkTemp2( Pan3Time )
    rele.desliga(Gas3)   
#Fim do aquecimento da panela 3

#Avisar o usuário para medir OG com refratômetro
#Notificar para dropar os graos

#Inicio do mashout
def mashOutInit():
    mashOut( Mashout )
    rele.desliga(Gas1)
#Fim do mashout

#Inicio da trasfega 1 (joga o liquido da panela 1 para a panela 2)
def trasfega1():
    rele.liga(BombaRT)
    checkTrasfega()                                 #Funcao para verificar se o volume parou de descer por pelo menos 5 min
    rele.desliga(BombaRT)
#Fim da Trasfega 1

#Inicio do Flysparge
def flysparge():
    Vol3 = 0
    rele.liga(BombaFlysparge) #Flysparge
    Vol3 = ultrassonico.MediaVolume(3)
    while Vol3 > 0.5: 
        Vol3 = ultrassonico.MediaVolume(3)
    rele.desliga(BombaFlysparge)
#Fim do flysparge

#Inicio da Trasfega 2 :
def trasfega2():
    rele.liga(BombaRT) #Trasfega2
    checkTrasfega()                                 #Funcao para verificar se o volume parou de descer por pelo menos 5 min
    rele.desliga(BombaRT)
#Fim da Trasfega 2

#notifica do chinook1

#Inicio da Fervura (panela 2):
def fervura( a, b ):
    checkTemp3( a, b, Fervura )                                    
    rele.desliga(Gas2)
#Fim da fervura

#notifica para adicionar o wirfloc
#Notifica para iniciar o whirpool por 10 min

#Apos os 70 min tem que mover a boca da bomba de trasfega para resfriador
#rele.liga(BombaRT)

#aguarda confimarcao de que todo o liquido saiu da panela 2

#rele.desliga(BombaRT) 
#Fim do processo


    #recirculaçao ocorre durante a mostruração, ou durante todo o processo ou apenas nos 15 min finais dos 60
    #apos a mosturacao, ocorre o mashout, apos isso ocorre a trasfega1, apos ela tem o flysparge da somente agua(agua quente na mesma temp da mosturacao)
    #apos flysparge, ocorre a trasfega2 da mosturacao para fervura
    #na fervura fica a 100° por tempo
    





