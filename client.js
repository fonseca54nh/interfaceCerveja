var ws = new WebSocket("ws://localhost:1337/");

function sendToServer( a )      { ws.send( JSON.stringify( a ) )              }
function parse(a)               { return JSON.parse( a )                      }
function select(a)              { return document.querySelector( '#' + a )    }
function selectClass(a)         { return document.getElementsByClassName( a ) }
function show(a)                { select( a ).style = 'display: ;'       }
function hide(a)                { select( a ).style = 'display: none;'        }
function style(a,b)             { select( a ).style = b                       }
function setInner(a,b)          { select( a ).innerText = b                   }
function setHTML(a,b)           { select( a ).innerHTML = b                   }
function getInner(a)            { return select( a ).innerText                }
function getName()              { return select( "uname" ).value 	      }
function getInnerFromClass(a,b) { return selectClass( a )[b].innerText        }
function disable( a )           { select( a ).disabled = true; select( a ).style = "background-color:grey;" }

// quando o cara abrir o servidor manda as receitas disponiveis
// entao mostrar as receitas na tela

ws.onopen = function(){
    console.log("Connection is Established");
    //ws.send("{}");
    selectRecipe()
    show( "message" )
    hide( "container" )
    hide( "ing"       )
};

function selectRecipe()
{
	for( let i = 0; i < selectClass( "recipe" ).length; ++i )
	{
		selectClass( "recipe" )[i].onclick = () =>
		{
			recipe = { type: 'selectedRecipe' }
			recipe.name = getInnerFromClass( "recipe", i )
		      	console.log( recipe )
			sendToServer( recipe )
		}
	}
}

let semaphore = 0

ws.onmessage = function(evt) {
        //var received_msg = evt.data;
        //console.log(received_msg);
	data = parse( evt.data )
	type = data.type
	console.log( "Type: " + type )
	if( type == "recipe" ) 
	{
		var ingredientes = data.ingredientes

		var stringo = " "

		var itemCounter = 0
		var counter     = 0

		var unidades = []

		for( item in ingredientes )
		{
			if( item == "unidades" )
			{
				for( var i in data.ingredientes[item] )
				{
					unidades.push( data.ingredientes[item][ i ] )
				}
			}else if( typeof( data.ingredientes[ item ] ) == 'object' )
			{
				var el   = document.createElement( "tr" )
				var indx = document.createElement( "th" )
				var type = document.createElement( "th" )
				var qtde = document.createElement( "th" )
				var unit = document.createElement( "th" )

				for( var i in data.ingredientes[ item ] )
				{
					if( item == "Malte" )
					{
						var el   = document.createElement( "tr" )
						var indx = document.createElement( "th" )
						var type = document.createElement( "th" )
						var qtde = document.createElement( "th" )
						var unit = document.createElement( "th" )

						indx.innerHTML = item
						type.innerHTML = Object.keys( data.ingredientes[item] )[counter]
						qtde.innerHTML = data.ingredientes[ item ][i]
						unit.innerHTML = data.ingredientes.unidades[item]
						qtde.id  = Object.keys( data.ingredientes[item] )[counter] + "Q"
						counter += 1

					}else
					{
						indx.innerHTML = item
						type.innerHTML = Object.keys( data.ingredientes[item] )[0]
						qtde.innerHTML = data.ingredientes[ item ][i]
						unit.innerHTML = data.ingredientes.unidades[item]
						qtde.id  = item + "Q"
						counter += 1

					}

					el.appendChild( indx )
					el.appendChild( type )
					el.appendChild( qtde )
					el.appendChild( unit )

					select( "ing" ).appendChild( el )

				}
				counter = 0
			}
			else
			{
				var el   = document.createElement( "tr" )
				var indx = document.createElement( "th" )
				var type = document.createElement( "th" )
				var qtde = document.createElement( "th" )
				var unit = document.createElement( "th" )

				indx.innerHTML = item
				type.innerHTML = "-"
				qtde.innerHTML = data.ingredientes[ item ]
				unit.innerHTML = data.ingredientes.unidades[item]
				qtde.id  = item + "Q"

				el.appendChild( indx )
				el.appendChild( type )
				el.appendChild( qtde )
				el.appendChild( unit )

				select( "ing" ).appendChild( el )
			}
		}

		show( "ing" );
		show( "message" );
		show( "container" );

		select( "minus" ).onclick = () =>
		{
			curr = select( "volumeinput" ).value
			newv = curr - 10
			counter = 0

			for( item in ingredientes )
			{
				if( item != "Volume" && item != "OG" && item != "unidades" && item != "Wirfloc")
				{
					for( var i in data.ingredientes[ item ] )
					{
						if ( item == "Malte" ) 
						{
							setHTML( Object.keys( data.ingredientes[item] )[counter] + "Q", 
								( newv * data.ingredientes[item][i] / 60 ).toFixed(2) )
							counter += 1
						}
						else
						{
							//select( item + "Q" ).innerHTML = ( newv * data.ingredientes[item][i] ).toFixed( 2 )
							setHTML( item + "Q", (newv * data.ingredientes[item][i]).toFixed( 2 ) )
						}
					}
				}
				else 
				{
					if ( item == "Volume" ) setHTML ( "VolumeQ", newv )
				}
			}
			select("volumeinput").value = newv
		}

		select( "plus" ).onclick = () =>
		{
			curr = select( "volumeinput" ).value
			newv = parseInt( curr ) + 10
			counter = 0

			for( item in ingredientes )
			{
				if( item != "Volume" && item != "OG" && item != "unidades" && item != "Wirfloc")
				{
					for( var i in data.ingredientes[ item ] )
					{
						if ( item == "Malte" ) 
						{
							setHTML( Object.keys( data.ingredientes[item] )[counter] + "Q", 
								( newv * data.ingredientes[item][i] / 60 ).toFixed(2) )
							counter += 1
						}
						else
						{
							//select( item + "Q" ).innerHTML = ( newv * data.ingredientes[item][i] ).toFixed( 2 )
							setHTML( item + "Q", ( newv * data.ingredientes[item][i] ).toFixed( 2 ) )
						}

					}
				}
				else 
				{
					if ( item == "Volume" ) setHTML ( "VolumeQ", newv )
				}
			}
			select( "volumeinput" ).value = newv
		}

		select( "confirm" ).onclick = () =>
		{

			start = { type: 'start' }
			start.value = "1"

			sendToServer( start )		
			hide( "ing"       )
			hide( "cancel"    )
			hide( "minus"     )
			hide( "volumeinput"    )
			hide( "plus"      )
			setHTML( "messageHeader", "Atencao" )
		}
	}

	if( type == "moverMangueiraParaAPanela3" )
	{
		semaphore = 1

		msg = "Mova a mangueira da Panela 1 para a Panela 3. Clique em confirmar quando pronto."
		answer = { type: 'mangueiraMovidaParaAPanela3' }
		setInner( "messageContent", msg )
		select( "confirm" ).onclick = () =>
		{
			sendToServer( answer )
			semaphore = 0
		}
	}

	if( type == "droparOsGraos" )
	{
		while( semaphore != 0 )
		{

		}
		msg = "Coloque na panela os graos de Malte, meca o OG e mude a mangueira da recircurlacao para trasfega. Clique em confirmar quando pronto."
		answer = { type: 'graosDropados' }
		setInner( "messageContent", msg )
		select( "confirm" ).onclick = () =>
		{
			sendToServer( answer )
		}

	}
	if( type == "adicionarWirfloc" )
	{

		msg = "Adicione as pastilhas de wirfloc. Clique em confirmar quando pronto."
		answer = { type: 'wirflocDroppado' }
		setInner( "messageContent", msg )
		select( "confirm" ).onclick = () =>
		{
			sendToServer( answer )
		}

	}
	if( type == "commenceWhirpool" )
	{
		msg = "Mova a mangueira da Panela 1 para a Panela 3. Clique em confirmar quando pronto."
		answer = { type: 'whirpoolFinalizado' }
		setInner( "messageContent", msg )
		select( "confirm" ).onclick = () =>
		{
			sendToServer( answer )
		}

	}
	if( type == "moverBocasDaTrasfegaParaResfriamento" )
	{
		msg = "Mova as bocas da trasfega para o resfriamento. Clique em confirmar quando pronto."
		answer = { type: 'bocasMovidas' }
		setInner( "messageContent", msg )
		select( "confirm" ).onclick = () =>
		{
			sendToServer( answer )
		}
	}

	if( type == "confirmacaoSemLiquido" )
	{
		msg = "Confirme que a panela esta vazia. Clique em confirmar quando pronto."
		answer = { type: 'noLiquido' }
		setInner( "messageContent", msg )
		select( "confirm" ).onclick = () =>
		{
			sendToServer( answer )
		}
	}
};

	
