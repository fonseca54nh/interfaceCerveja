import threading
import os
import time
from time import sleep
import RPi.GPIO as GPIO           # import RPi.GPIO module  

#print('Portas: 17 Centelhador - 13 - 19 gas2 - 26 - 12 gas3 - 16 Bomba- 20 gas1 - 21 Agua')

def setup():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)            # choose BCM or BOARD  
    #GPIO.setup(port_or_pin, GPIO.OUT) # set a port/pin as an output   
    #GPIO.output(port_or_pin, 1)       # set port/pin value to 1/GPIO.HIGH/True  
    #GPIO.output(port_or_pin, 0)       # set port/pin value to 0/GPIO.LOW/False  


    GPIO.setup(17, GPIO.OUT) #CENTELHADOR
    GPIO.setup(13, GPIO.OUT) #NAO USA porem vai ser para a bomba da flysparge
    GPIO.setup(19, GPIO.OUT) #GAS 2
    GPIO.setup(26, GPIO.OUT) #NAO USA 
    GPIO.setup(12, GPIO.OUT) #GAS 3
    GPIO.setup(16, GPIO.OUT) #BOMBA RECIR
    GPIO.setup(20, GPIO.OUT) #GAS 1
    GPIO.setup(21, GPIO.OUT) #AGUA


    #Inicia Desligado---------------------------------------------
    GPIO.output(17, 1)                #Inicia com eles Desligados
    GPIO.output(13, 1)                #Inicia com eles Desligados
    GPIO.output(19, 1)                #Inicia com eles Desligados  
    GPIO.output(26, 1)                #Inicia com eles Desligados
    GPIO.output(12, 1)                #Inicia com eles Desligados
    GPIO.output(16, 1)                #Inicia com eles Desligados
    GPIO.output(20, 1)                #Inicia com eles Desligados
    GPIO.output(21, 1)                #Inicia com eles Desligados

def liga(pin):
    GPIO.output(pin,0)

def desliga(pin):
    GPIO.output(pin,1)

