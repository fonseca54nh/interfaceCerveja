#!/usr/bin/env python

import asyncio
import websockets
import json

#import sensors
#import rele

import threading as t

terminoAquecimentoPanela3 = 0
bombaTrasfega = 0

#sensors.MostTemp  = 1
#sensors.recircula = 15
#sensors.Pan3Time  = 15
#sensors.Mashout   = 15
#sensors.Fervura   = 60

moverMangueiraParaAPanela3           = "{\"type\": \"moverMangueiraParaAPanela3\" }"
droparOsGraos                        = "{\"type\": \"droparOsGraos\" }"
adicionarWirfloc                     = "{\"type\": \"adicionarWirfloc\" }"
commenceWhirpool                     = "{\"type\": \"commenceWhirpool\" }"
moverBocasDaTrasfegaParaResfriamento = "{\"type\": \"moverBocasDaTrasfegaParaResfriamento\" }"
confirmacaoSemLiquido                = "{\"type\": \"confirmacaoSemLiquido\" }"


terminoAquecimentoPanela3 = 0

def panela3():
    print( "abastecimento panela 3" )
    #sensors.abastecimentoPanela3()
    print( "abastecimento panela 3 finito" )
    print( "aquecimento panela 3" )
    #sensors.aquecimentoPanela3()
    terminoAquecimentoPanela3 = 1
    print( "aquecimento panela 3 finito" )


async def handler(websocket):
    while True:
        message = await websocket.recv()
        message = json.loads( message )
        #print( message["name"])
    

        if( message[ "type" ] == "selectedRecipe" ):
            if( message["name"] == "Belgian Blond" ):
                #message = "{\"type\": \"recipe\", \"content\": \"" +  + "\"}"
                await websocket.send( open( "belgianBlond.json", "r" ) )
                #await websocket.send( message )

        if( message[ "type" ] == "start" ):
            if( message["value"] == "1" ):
                #sensors.abastecimentoPanela1()
                await websocket.send( moverMangueiraParaAPanela3 )

                mesg = await websocket.recv()
                mesg = json.loads( mesg )
                while( mesg["type"] != "mangueiraMovidaParaAPanela3" ):
                        print("A")

                if( mesg["type"] == "mangueiraMovidaParaAPanela3" ):
                    t1 = t.Thread( target=panela3 )
                    t1.start()

                print( "vou comecar mosturacao agora brace yourselves!" )
                #sensors.mosturacaoInit()
                print( "mosturacao finita" )
                await websocket.send( droparOsGraos )

        # verificar isso
        if( message[ "type" ] == "bombaTrasfegaMudada" ):
            bombaTrasfega = 1

        if( message[ "type" ] == "graosDropados" ):
            print( "mash out init" ) # paramos aqui
            sensors.mashOutInit()
            while(1):
                if( bombaTrasfega == 1 ):
                    sensors.trasfega1()
                    break
            while( 1 ):
                if( terminoAquecimentoPanela3 == 1 ):
                    sensors.flysparge()
                    break
            sensors.trasfega2()
            sensors.fervura( 60, 15 )
            await websocket.send( "adicionarWirfloc" )

        if( message[ "type" ] == "wirflocDroppado" ):
            await websocket.send( "commenceWhirpool" ) #mandar msg no cliente avisando q terminou o whirpool

        if( message[ "type" ] == "whirpoolFinalizado" ):
            await websocket.send( "moverBocasDaTrasfegaParaResfriamento" )

        if( message["type"] == "bocasMovidas" ):
            rele.liga( sensors.BombaRT )
            await websocket.send( "confirmacaoSemLiquido" )

        if( message["type"] == "noLiquido" ):
            rele.desliga( sensors.BombaRT ) 

async def main():
    async with websockets.serve(handler, "localhost", 1337):
        await asyncio.Future()  # run forever

if __name__ == "__main__":
    asyncio.run(main())
