#!/usr/bin/env python
"""
================================================
ABElectronics ADC Pi 8-Channel ADC demo

Requires python smbus to be installed
run with: python demo_readvoltage.py
================================================

Initialise the ADC device using the default addresses and sample rate,
change this value if you have changed the address selection jumpers

Sample rate can be 12,14, 16 or 18
"""

from __future__ import absolute_import, division, print_function, \
                                                    unicode_literals
import time
import os
import math



def setup():
    try:
        from ADCPi import ADCPi
    except ImportError:
        print("Failed to import ADCPi from python system path")
        print("Importing from parent folder instead")
        try:
            import sys
            sys.path.append('..')
            from ADCPi import ADCPi
        except ImportError:
            raise ImportError(
                "Failed to import library from parent folder")

   



def calcTemp(resistance):
    #Beta coeficientes
    t25Resistance = 10000
    bResistance = 3950
    t0 = 273.15
    t25 = t0 + 25.0

    return 1 / ( (math.log(resistance / t25Resistance) / bResistance) + (1 / t25) ) - t0;            


            
def temp(num):
    '''
    Main program function
    '''
    from ADCPi import ADCPi
    adc = ADCPi(0x68, 0x69, 12)

    #constantes
    V_cc = 5.0
    R_1 = 10000.0
    R_2 = 6800.0
    

    

    #Steinhart–Hart coeficientes
    a = 283786.2
    b = 0.06593
    c = 49886.0

        
    #sum_voltage = []
    #sum_resistermistor = []

    # clear the console
    


    #Tensão na Divisão de Tensão------------------------------------------------------------------------
    x = 0
    V_0S = 0
    V_0M = 0
    if num == 1: #para panela 1
        while (x < 25):
            V_0 = adc.read_voltage(5)       #Esse que tem que mudar para o pino das portas analogicas do ADC pi (Esse é a pino 5) fio preto terra, o fio vermelho vai no 5v
            V_0S = V_0S + V_0
            x = x + 1
        V_0M = V_0S/25
    elif num == 2: #para panela 2
        while (x < 25):
            V_0 = adc.read_voltage(6)       #Esse que tem que mudar para o pino das portas analogicas do ADC pi (Esse é a pino 5) fio preto terra, o fio vermelho vai no 5v
            V_0S = V_0S + V_0
            x = x + 1
        V_0M = V_0S/25
    elif num == 3: #para panela 3
        while (x < 25):
            V_0 = adc.read_voltage(7)       #Esse que tem que mudar para o pino das portas analogicas do ADC pi (Esse é a pino 5) fio preto terra, o fio vermelho vai no 5v
            V_0S = V_0S + V_0
            x = x + 1
        V_0M = V_0S/25

    #print("tensao termistor: %.2f V" % V_0M)
    
    
    #resistencia do Termistor------------------------------------------------------------------------
    
    resistermistor = ((16800*V_cc) - (16800*V_0))/(V_0M)
    
    #print("resistencia termistor: %.2f Ohm" % resistermistor)


    #temperatura------------------------------------------------------------------------
    #Beta Resistence
    TempBeta = calcTemp(resistermistor)
    #print("Temperatura: %.2f ºC" % TempBeta)
    #print("Temperatura:",round(TempBeta, 2),"ºC ","Resistencia_Termistor:",round(resistermistor, 2),"Tensao_Termistor: ",round(V_0M, 2))
    
    #print(str(round(TempBeta, 2))   +  "\n" + str(round(resistermistor, 2)) + "\n" + str(round(V_0M,2))) Essa aqui que a gente quer!!!!!!!!!!

    #Steinhart–Hart
    #aux = ((R_1*voltage)/(a*(V_0-voltage)))-(c/a)
    #print("Auxiliar: %.2f " % aux)
    #TempStein = (-1/b)*math.log(aux)
    #print("Temperatura: %.2f C" % TempStein)


    # wait 0.2 seconds before reading the pins again
    time.sleep(0.4)
    return round(TempBeta, 2) 
